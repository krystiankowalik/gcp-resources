# Secure Ingress with SSL

##Cert-manager with letsencrypt

### 1. Import cert-manager resources

```shell
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.crds.yaml
```

### 2. Create cert-manager namespace

```shell
kubectl create namespace cert-manager
```

### 3. Deploy cert-manager with helm

```shell
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.7.1
```

### 4. Deploy ingress-controller

```shell
helm upgrade --install ingress-nginx ingress-nginx \
--repo https://kubernetes.github.io/ingress-nginx \
--namespace ingress-nginx --create-namespace
```

### 5. Deploy ClusterIssuer

```shell
kubectl apply -f ingress/cert-manager/clusterissuer.yaml 
```

### 6. Deploy your app (with ingress)
For example:
```shell
helm repo add myhelmrepo https://krystiankowalik.github.io/helm-chart/
helm repo update
helm install sample-cloud-config-server myhelmrepo/sample-cloud-config-server --version=0.1.16 -n development 
```